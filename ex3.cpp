#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;

int julianday(int month, int year, int day)
{
    int a, m, y, leap_days, jdn;
    a = (14 - month) / 12;
    m = (month - 3) + (12 * a);
    y = year + 4800 - a;
    leap_days = (y / 4) - (y / 100) + (y / 400);
    jdn = day + (((153 * m) + 2) / 5) + (365 * y) + leap_days - 32045;
    return jdn; // return the equation that calculate the julian day from any date (day/month/year)
}

int main()
{
    string day, month, year, maxT, minT;
    string line;
    ifstream infile;
    ofstream outfile;
    infile.open("SevilleTemp1960-2020.txt"); // opens the input file
    outfile.open("sevillanew.txt");          // opens the output file
    outfile << "Day;month;year;JulianDay;MaxTemp;MinTemp" << '\n';
    if (infile.is_open()) {
        while (getline(infile, line, '\n')) { // to read the file line by line

            if (isalpha(line[0]))
                continue; // to ignore the reading of the (alphabets) we have in
                          // the title

            stringstream s(line);
            string d;
            getline(s, d, '\t');
            if (d == "") {
                getline(infile, line, '\n');

                continue;
            } // to ignore the empty spaces(continue) then to read directy the
            // characters from the file
            stringstream wholedate(d);
            getline(wholedate, day,
                    '-'); // read from the line the first data delimited by '-'
            getline(wholedate, month,
                    '-'); // read from the line the second data delimited by '-'
            // which gives the month data in our case
            getline(wholedate, year);
            getline(s, maxT,
                    '\t'); // read from the line the fourth data delimited by a space
            getline(s, minT);

            outfile << day << ';' << month << ';' << year << ';'
                    << julianday(stoi(month), stoi(day), stoi(year)) << ';' << stod(maxT) << ';'
                    << stod(minT) << '\n'; // write the data of the file we read in the new file
            // stoi converts the string into an integer and stod converts the string
            // into a float (as asked in the exercice)
        }

        infile.close();
        outfile.close();
    } else
        cout << "unable to open the file \n";

    return 0;
}
