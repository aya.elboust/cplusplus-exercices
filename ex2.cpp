#include <cmath>
#include <iostream>
using namespace std;
double f(double);
double interpolated_f(double); // declaration of the functions

double f(double x)
{
    return sin(x * x);
} // the function we will apply the interpolation on

double interpolated_f(double x0, double y0, double x1, double y1, double x)
{
    return y0 + ((y1 - y0) / (x1 - x0)) * (x - x0);
} // returns the expression of the linear interpolation between two points

int main()
{
    double a[5], b[5];
    int N;
    cout << "enter N : \n";
    cin >> N;                   // asking the user to enter the number N of new points
    for (int i = 0; i < 5; i++) // producing an initial dataset of 5 points (x,f(x))
    {
        a[i] = i;
        b[i] = f(i);
    }
    for (int j = 0; j < 4; j++) // we have 5 points so we need 4 integrals
    {
        for (double i = j; i < j + 1;
             i += 1.0 / N) // inside each interval there's N iterpolated points
        {
            cout << i << ";" << interpolated_f(a[j], b[j], a[j + 1], b[j + 1], i)
                 << '\n'; // display the new points by calling the function of
                          // interpolation from above
        }
    }

    return 0;
}
