
#include <cmath>
#include <iostream>
using namespace std;
double f(double); // functions declaration
double bisection(double);

double f(double x)
{
    return sin(x * x);
} // the function that we will apply the bissection method on

double bisection(double a, double b, double epsilon)
{
    int n = 0;
    double c = a;
    while ((b - a) >= epsilon) // while the interval length is bigger that the
    // tolerance value do ...(in order to have a real
    // small interval and approach the solution)
    {
        c = (a + b) / 2; // we subdivise the interval
        if (f(c) == 0.0)
            break; // if we reached the soluton we don't need to go further

        else if (f(c) * f(a) < 0)
            b = c; // to make the interva smaller

        else
            a = c; // to make the interval smaller
        n++;
    }
    cout << "the number of iteration done by the algorithm is:" << n << '\n';
    cout << "the solution with the bisection method is x0=" << c << '\n';

    return c;
}

int main()
{
    double x, y, ep, z;
    string desire;
    cout << "enter the value a:" << '\n';
    cin >> x;
    cout << "enter the value b:" << '\n';
    cin >> y;
    cout << "Enter the desired tolerance value:" << '\n';
    cin >> ep;
    z = bisection(x, y, ep);
    cout << "the image of the solution is f(a)=" << f(z) << '\n';
    cout << "Do you want to do another calculation?(yes or no)" << '\n';
    cin >> desire;
    if (desire == "yes")
        cout << "What is the type of calculations you want to do?" << '\n';
    else if (desire == "no")
        exit(0);
    return 0;
}
